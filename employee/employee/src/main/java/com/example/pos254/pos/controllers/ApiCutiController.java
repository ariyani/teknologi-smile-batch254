package com.example.pos254.pos.controllers;


import com.example.pos254.pos.Repository.CutiRepo;
import com.example.pos254.pos.models.Cuti;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCutiController {

    @Autowired
    private CutiRepo cutiRepo;

    @GetMapping("/cuti")
    public ResponseEntity<List<Cuti>> GetAllCuti(){
        try{
            List<Cuti> cuti = this.cutiRepo.findAll();
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/cuti")
    public ResponseEntity<Object> SaveCuti(@RequestBody Cuti cuti){
        Cuti cutiData=this.cutiRepo.save(cuti);
        try{
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        } catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/cuti/{id}")
    public ResponseEntity<List<Cuti>> GetCutiById(@PathVariable("id") Long id){
        try {
            Optional<Cuti> cuti = this.cutiRepo.findById(id);
            if (cuti.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(cuti, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }
            catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }
    @PutMapping("/cuti/{id}")
    public ResponseEntity<List<Cuti>> UpdateCuti(@PathVariable("id") Long id, @RequestBody Cuti cuti){
        Optional<Cuti> cutiData = this.cutiRepo.findById(id);
        if(cutiData.isPresent()){
            cuti.setId(id);
            this.cutiRepo.save(cuti);
            ResponseEntity rest = new ResponseEntity<>(cuti,  HttpStatus.OK);
            return rest;
        }
        else
        {
            return  ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/cuti/{id}")
    public ResponseEntity<Object> DeleteCuti(@PathVariable ("id") Long id){
        this.cutiRepo.deleteById(id);
        return  new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
