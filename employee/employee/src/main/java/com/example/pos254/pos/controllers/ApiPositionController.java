package com.example.pos254.pos.controllers;


import com.example.pos254.pos.Repository.PositionRepo;
import com.example.pos254.pos.models.Position;
import com.example.pos254.pos.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")

public class ApiPositionController {
    @Autowired
    private PositionRepo positionRepo;

    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("position/index");
        return view;
    }

    @GetMapping("/position") // Buat apa?
    public ResponseEntity<List<Position>> GetAllPosition() {
        try {
            List<Position> category = this.positionRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @PostMapping("/position")
    public ResponseEntity<Object> SavePosition(@RequestBody Position position) {
        Position positionData = this.positionRepo.save(position);
        try {
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/position/{id}")
    public ResponseEntity<List<Position>> GetPositionById(@PathVariable("id") Long id) {
        try {
            Optional<Position> position = this.positionRepo.findById(id);
            if (position.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(position, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/position/{id}")
    public ResponseEntity<Object> UpdatePosition(@RequestBody Position position, @PathVariable("id") Long id) {
        Optional<Position> positionData = this.positionRepo.findById(id);
        if (positionData.isPresent()) {
            position.setId(id);
            this.positionRepo.save(position);

            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/position/{id}")
    public ResponseEntity<Object> DeleteRole(@PathVariable("id") Long id) {
        this.positionRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    //Controller for Search
    @GetMapping("/searchbyposition/{keyword}") // name yang dikiirm dari depan, lihat keyword isi atau ngga sesuai category berdasarkan name
    public ResponseEntity<List<Position>> getPositionByName(@PathVariable("keyword") String keyword){
        if (keyword != null){
            List<Position> position = this.positionRepo.SearchByPosition(keyword);
            return new ResponseEntity<>(position, HttpStatus.OK);
        }
        else {
            List<Position> position = this.positionRepo.findAll();
            return new ResponseEntity<>(position, HttpStatus.OK);
        }
    }

    @GetMapping("positionmapped")
    public ResponseEntity<Map<String, Object>> getAllpage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size){
        try{
            List<Position> position = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Position> pageTuts;

            pageTuts = positionRepo.findAll(pagingSort);

            position = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();

            response.put("position", position);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return  new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}