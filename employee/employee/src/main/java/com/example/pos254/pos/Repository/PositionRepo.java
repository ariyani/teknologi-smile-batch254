package com.example.pos254.pos.Repository;

import com.example.pos254.pos.models.Position;
import com.example.pos254.pos.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.*;
import java.util.List;


public interface PositionRepo extends JpaRepository<Position,Long> {
    @Query("FROM Position WHERE lower(PositionName) LIKE lower(concat('%', ?1, '%'))")
    public List<Position> SearchByPosition(String keyword);

}
