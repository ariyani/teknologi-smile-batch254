package com.example.pos254.pos.controllers;


import com.example.pos254.pos.Repository.RoleRepo;
import com.example.pos254.pos.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@Controller("*")
@RequestMapping("/api")
public class ApiRoleController {

    @Autowired
    private RoleRepo roleRepo;


    @GetMapping("/role") // Buat apa?
    public ResponseEntity<List<Role>> GetAllRole() {
        try {
            List<Role> role= this.roleRepo.findAll();
            return new ResponseEntity<>(role, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/role")
    public ResponseEntity<Object> SaveRole(@RequestBody Role role) {
        Role roleData = this.roleRepo.save(role);
        try {
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/role/{id}")
    public ResponseEntity<List<Role>> GetRoleById(@PathVariable("id") Long id) {
        try {
            Optional<Role> role = this.roleRepo.findById(id);
            if (role.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/role/{id}")
    public ResponseEntity<Object> UpdateRole(@RequestBody Role role, @PathVariable("id") Long id) {
        Optional<Role> roleData = this.roleRepo.findById(id);
        if (roleData.isPresent()) {
            role.setId(id);
            this.roleRepo.save(role);

            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/role/{id}")
    public ResponseEntity<Object> DeleteRole(@PathVariable("id") Long id){
        this.roleRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    //Controller for Search
    @GetMapping("/searchbyrole/{keyword}") // name yang dikiirm dari depan, lihat keyword isi atau ngga sesuai category berdasarkan name
    public ResponseEntity<List<Role>> getRoleByName(@PathVariable("keyword") String keyword){
        if (keyword != null){
            List<Role> role = this.roleRepo.searchByRole(keyword);
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
        else {
            List<Role> category = this.roleRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }
    }

    @GetMapping("rolemapped")
    public ResponseEntity<Map<String, Object>> getAllpage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size){
        try{
            List<Role> role = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Role> pageTuts;

            pageTuts = roleRepo.findAll(pagingSort);

            role = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();

            response.put("role", role);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return  new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
