package com.example.pos254.pos.models;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cuti")
public class Cuti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private long Id;

    @ManyToOne
    @JoinColumn(name = "employee_id", insertable = false, updatable = false)
    private Employee employee;

    @Column(name = "employee_id")
    private Long EmployeeId;

    @Column(name = "jenis_cuti")
    private String JenisCuti;

    @Column(name = "tanggal_mulai")
    private Date TanggalMulai;

    @Column(name = "tanggal_selesai")
    private Date TanggalSelesai;

    @Column(name = "lama_cuti")
    private int LamaCuti;

    @Column(name = "alasan_cuti")
    private String AlasanCuti;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Long getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Long employeeId) {
        EmployeeId = employeeId;
    }

    public String getJenisCuti() {
        return JenisCuti;
    }

    public void setJenisCuti(String jenisCuti) {
        JenisCuti = jenisCuti;
    }

    public Date getTanggalMulai() {
        return TanggalMulai;
    }

    public void setTanggalMulai(Date tanggalMulai) {
        TanggalMulai = tanggalMulai;
    }

    public Date getTanggalSelesai() {
        return TanggalSelesai;
    }

    public void setTanggalSelesai(Date tanggalSelesai) {
        TanggalSelesai = tanggalSelesai;
    }

    public int getLamaCuti() {
        return LamaCuti;
    }

    public void setLamaCuti(int lamaCuti) {
        LamaCuti = lamaCuti;
    }

    public String getAlasanCuti() {
        return AlasanCuti;
    }

    public void setAlasanCuti(String alasanCuti) {
        AlasanCuti = alasanCuti;
    }
}