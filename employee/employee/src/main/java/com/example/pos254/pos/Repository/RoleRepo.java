package com.example.pos254.pos.Repository;

import com.example.pos254.pos.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepo  extends JpaRepository <Role, Long> {

    @Query("FROM Role WHERE lower(RoleName) LIKE lower(concat('%', ?1, '%'))")
    public List<Role> searchByRole(String keyword);
}
