package com.example.pos254.pos.models;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long Id;

    @Column(name = "nip")
    private String Nip;

    @Column(name = "name")
    private String Name;

    @Column(name = "dob")
    private Date Dob;

    @Column(name = "pob")
    private String Pob;

    @Column(name = "nohp")
    private String Nohp;

    @Column(name = "email")
    private String Email;

    @ManyToOne
    @JoinColumn(name = "position_id", insertable = false, updatable = false)
    public Position position;

    @Column(name = "position_id")
    private long PositionId;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    public Role role;

    @Column(name = "role_id")
    private long RoleId;

    @Column(name = "agama")
    private String Agama;

    @Column(name = "sisa_cuti")
    private long SisaCuti=12;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNip() {
        return Nip;
    }

    public void setNip(String nip) {
        Nip = nip;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Date getDob() {
        return Dob;
    }

    public void setDob(Date dob) {
        Dob = dob;
    }

    public String getPob() {
        return Pob;
    }

    public void setPob(String pob) {
        Pob = pob;
    }

    public String getNohp() {
        return Nohp;
    }

    public void setNohp(String nohp) {
        Nohp = nohp;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public long getPositionId() {
        return PositionId;
    }

    public void setPositionId(long positionId) {
        PositionId = positionId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public long getRoleId() {
        return RoleId;
    }

    public void setRoleId(long roleId) {
        RoleId = roleId;
    }

    public String getAgama() {
        return Agama;
    }

    public void setAgama(String agama) {
        Agama = agama;
    }

    public long getSisaCuti() {
        return SisaCuti;
    }

    public void setSisaCuti(long sisaCuti) {
        SisaCuti = sisaCuti;
    }
}