package com.example.pos254.pos.Repository;


import com.example.pos254.pos.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee,Long> {
    @Query("FROM Employee WHERE lower(Name) LIKE lower(concat('%',?1,'%'))")
    List<Employee> SearchEmployee(String keyword);
}
