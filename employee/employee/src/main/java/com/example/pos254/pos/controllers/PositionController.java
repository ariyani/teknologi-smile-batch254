package com.example.pos254.pos.controllers;

import com.example.pos254.pos.Repository.PositionRepo;
import com.example.pos254.pos.Repository.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping( value="position")

public class PositionController {
    @Autowired
    private PositionRepo positionrepo;

    @GetMapping(value= "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("position/index");
        return view;
    }
}
