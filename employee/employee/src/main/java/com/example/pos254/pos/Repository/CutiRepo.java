package com.example.pos254.pos.Repository;

import com.example.pos254.pos.models.Cuti;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CutiRepo extends JpaRepository<Cuti, Long> {
}
