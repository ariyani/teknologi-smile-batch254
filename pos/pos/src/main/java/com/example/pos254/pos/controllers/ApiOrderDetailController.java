package com.example.pos254.pos.controllers;


import com.example.pos254.pos.Repository.OrderDetailRepo;
import com.example.pos254.pos.Repository.OrderHeaderRepo;
import com.example.pos254.pos.models.Category;
import com.example.pos254.pos.models.OrderDetail;
import com.example.pos254.pos.models.OrderHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderDetailController {
    @Autowired
    private OrderDetailRepo orderdetailRepo;

    @GetMapping("/orderdetail")
    public ResponseEntity<List<OrderDetail>> GetAllOrderDetail()
    {
        try{
            List<OrderDetail> orderDetail = this.orderdetailRepo.findAll();
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping ("/orderdetailbyorder/{id}")
    public ResponseEntity<List<OrderDetail>> GetAllOrderById(@PathVariable("id") Long id) {
        try {
            List<OrderDetail> orderDetail = this.orderdetailRepo.FindByHeaderId(id);
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @PostMapping("/orderdetail")
    public ResponseEntity<Object> SaveOrderDetail(@RequestBody OrderDetail orderdetail) {

        OrderDetail orderDetailData = this.orderdetailRepo.save(orderdetail);
//        if (orderDetailData.equals(orderdetail)) {
//            return new ResponseEntity<>("Save Success", HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>("Save Failed!!!", HttpStatus.BAD_REQUEST);
//        }

        try {
            orderdetail.setCreatedBy("name");
            orderdetail.setCreatedOn(new Date());
            this.orderdetailRepo.save(orderdetail);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping("/orderdetail/{id}")
    public ResponseEntity<Object> UpdateOrderDetail(@RequestBody OrderDetail orderdetail, @PathVariable("id") Long id) {
        Optional<OrderDetail> orderDetailData = this.orderdetailRepo.findById(id);
        if (orderDetailData.isPresent())
        {
            orderdetail.setId(id);
            orderdetail.setModifieBy("name");
            orderdetail.setModifieOn(new Date());
            this.orderdetailRepo.save(orderdetail);

            ResponseEntity rest = new ResponseEntity<>("Update Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/orderdetail/{id}")
    public ResponseEntity<Object> DeleteOrderDetail(@PathVariable("id") Long id)
    {
        this.orderdetailRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}

