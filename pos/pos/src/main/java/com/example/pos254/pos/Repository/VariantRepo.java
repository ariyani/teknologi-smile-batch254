package com.example.pos254.pos.Repository;

import com.example.pos254.pos.models.Variant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VariantRepo extends JpaRepository<Variant,Long> {
    @Query("FROM Variant WHERE CategoryId = ?1") //?1 refer ke categoryId
    List<Variant> FindByCategoryId(Long categoryId);
    @Query("FROM Variant WHERE lower(VariantName) LIKE lower(concat('%',?1,'%')) ")
    public List<Variant> SearchVariatn(String keyword);
}
