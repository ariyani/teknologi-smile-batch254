package com.example.pos254.pos.Repository;

import com.example.pos254.pos.models.Product;
import com.example.pos254.pos.models.Variant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepo extends JpaRepository <Product,Long> {
    @Query("FROM Product WHERE VariantId = ?1") //?1 refer ke categoryId
    List<Product> FindByProductId(Long id);

    @Query("FROM Product WHERE lower(ProductName) LIKE lower(concat('%',?1,'%')) ")
    public List<Product> SearchProduct(String keyword);
}
