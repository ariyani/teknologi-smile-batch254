package com.example.pos254.pos.Repository;

import com.example.pos254.pos.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryRepo extends JpaRepository<Category,Long> {
    // QUery Search by Name

    @Query ("FROM Category WHERE lower(CategoryName) LIKE lower(concat('%', ?1, '%'))") // Category name dicari berdasarkan name, bisa di ganti ganti
    public List<Category> searchByCategory(String keyword);
}
